import scalaj.http.Http

object Counter {
  var requestNumber = 0

  def addRequest(){
    synchronized{
      requestNumber += 1
    }
  }

  def removeRequest(){
    synchronized{
      requestNumber -= 1
    }
  }
}

class RequestThread(fileName: String) extends Runnable {
  def run(){
    // val response = Http("http://127.0.0.1:8000").responseCode
    val response = Http("http://127.0.0.1:8000/" + fileName).responseCode
    // val response = Http("http://127.0.0.1:8000/pdf.pdf").responseCode
    // println(response)
    Counter.removeRequest()
    // println(Counter.requestNumber)
  }
}

object Client {
  def main(args: Array[String]) = {
    var param = ""
    if (args.length > 0){
      param = args(0)
    }
    while (true){
      if (Counter.requestNumber < 10){
        Counter.addRequest()
        val request = new Thread(new RequestThread(param))
        request.start()
      }else{}
      Thread.sleep(5)
    }
  }
}